<?php
/**
 * Chrome admin form
 *
 * Dedicated Chrome form to dynamically show/ hide fields
 *
 * @since 1.0.9
 * @package head-browser-app-link-metas
 */
defined('HBALM_ADMIN') OR die();


if ( !current_user_can('manage_options') )
{
	wp_die('Access denied');
}
?>
<div class="wrap">
	<h1><?php _e( 'App icon generator Settings', 'hbalm' ); ?></h1>
	<hr />
	<div id="hbalm-main">
		<h2><?php _e( 'Chrome webapp settings', 'hbalm' ); ?></h2>
		<form action="options.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
			<?php settings_fields('hbalm_chrome'); ?>

			<table class="form-table">
				<tr>
					<th scope="row"> <?php _e( 'Create Chrome mobile webmanifest file', 'hbalm' ); ?></th>
					<td> <?php self::chrome_webmanifest_active_input(); ?> </td>
				</tr>
			</table>

			<div class="chrome-webmanifest-options-container" style="display: <?php echo $this->chrome_webmanifest_active ? 'block' : 'none;'; ?>">
				<table class="form-table">
					<tr>
						<th scope="row"> <?php _e( 'Chrome bg color', 'hbalm' ); ?></th>
						<td> <?php self::add_color_input(array('section' => 'hbalm_chrome_settings', 'setting' => 'chrome_bg_color')); ?> </td>
					</tr>

					<tr>
						<th scope="row"> <?php _e( 'Chrome app color', 'hbalm' ); ?></th>
						<td> <?php self::add_color_input(array('section' => 'hbalm_chrome_settings', 'setting' => 'chrome_color')); ?> </td>
					</tr>

					<tr>
						<th scope="row"> <?php _e( 'Chrome app orientation', 'hbalm' ); ?></th>
						<td> <?php self::chrome_orientation_input(); ?> </td>
					</tr>

					<tr>
						<th scope="row"> <?php _e( 'Chrome app display type', 'hbalm' ); ?></th>
						<td> <?php self::chrome_display_type_input(); ?> </td>
					</tr>
				</table>
			</div>


			<?php wp_nonce_field('update_settings', 'hbalm_nonce'); ?>
			<?php /*submit_button();*/ ?>
			<input class="button button-primary" type="submit" name="submit" value="<?php _e( 'Save', 'hbalm' ); ?>" />
		</form>
	</div>

	<?php $this->print_sidebar('hbalm_chrome'); ?>

</div>