<?php
/**
 * Metro admin form
 *
 * Dedicated Metro form to dynamically show/ hide fields
 *
 * @since 1.0.9
 * @package head-browser-app-link-metas
 */
defined('HBALM_ADMIN') OR die();


if ( !current_user_can('manage_options') )
{
	wp_die('Access denied');
}
?>
<div class="wrap">
	<h1><?php _e( 'App icon generator Settings', 'hbalm' ); ?></h1>
	<hr />
	<div id="hbalm-main">
		<h2><?php _e( 'Metro tile settings', 'hbalm' ); ?></h2>
		<form action="options.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
			<?php settings_fields('hbalm_metro'); ?>

			<table class="form-table">
				<tr>
					<th scope="row"> <?php _e( 'Tile bg color', 'hbalm' ); ?></th>
					<td> <?php self::add_color_input(array('section' => 'hbalm_metro_settings', 'setting' => 'metro_tile_color')); ?> </td>
				</tr>

				<tr>
					<th scope="row"> <?php _e( 'Metro HD icons', 'hbalm' ); ?></th>
					<td> <?php self::metro_hd_icon_active_input(); ?> </td>
				</tr>

				<tr>
					<th scope="row"> <?php _e( 'Force Metro TileImage metateg in HTML code', 'hbalm' ); ?></th>
					<td> <?php self::metro_html_tile_active_input(); ?> </td>
				</tr>

				<tr>
					<th scope="row"> <?php _e( 'Create IE/ Edge browserconfig.xml file', 'hbalm' ); ?></th>
					<td> <?php self::browserconfig_active_input(); ?> </td>
				</tr>
			</table>


			<table class="form-table">
				<tr>
					<th scope="row"> <?php _e( 'Metro wide icon', 'hbalm' ); ?></th>
					<td> <?php self::metro_wide_icon_active_input(); ?> </td>
				</tr>
			</table>

			<div class="metro-wide-tile-image-container" style="display: <?php echo $this->metro_wide_icon_active ? 'block' : 'none;'; ?>">
				<table class="form-table">
					<tr>
						<th scope="row"> <?php _e( 'Metro wide icon image', 'hbalm' ); ?></th>
						<td> <?php self::metro_wide_icon_image_input(); ?> </td>
					</tr>
				</table>
			</div>


			<?php wp_nonce_field('update_settings', 'hbalm_nonce'); ?>
			<?php /*submit_button();*/ ?>
			<input class="button button-primary" type="submit" name="submit" value="<?php _e( 'Save', 'hbalm' ); ?>" />
		</form>
	</div>

	<?php $this->print_sidebar('hbalm_metro'); ?>

</div>