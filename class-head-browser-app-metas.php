<?php
/*
Plugin Name: App icon generator
Plugin URI: https://bitbucket.org/ComeCaramelos/head-browser-app-links-metas/
Description: Creates all size site icons for Chrome, iOS/Safari, IE/Edge/Metro and Firefox (among others) using WordPress logo image and sets up your site as a standalone mobile application
Author: Roberto Solano
Author URI: https://comecaramelos.es/
Version: 1.1.2
Text Domain: hbalm
Domain Path: /languages
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


defined( 'ABSPATH' ) or die(); // Silence is gold

define( 'HBALM_BASENAME', plugin_basename( __FILE__ ) );
define( 'HBALM_URL', plugin_dir_url( __FILE__ ) );
define( 'HBALM_ABSPATH', plugin_dir_path( __FILE__ ) );
define( 'HBALM_RELPATH', basename( dirname( __FILE__ ) ) );
define( 'HBALM_FILE', __FILE__ );


/**
 * Base Class - App icon generator
 */
class Head_Browser_App_Links_Metas
{
	// Plugin version to avoid cache
	protected $plugin_version = '1.1.2';

	// File locations
	protected
		$webmanifest_fullpath,
		$browserconfig_fullpath;

	// Private variables
	private
		$image_id,
		$default_icons_sizes,
		$apple_icons_sizes,
		$metro_icons_sizes,
		$chrome_icons_sizes;

	// Admin variables
	protected
		$default_bg_color,
		$default_hd_active,
		$default_webmanifest_active,
		$apple_bg_color,
		$apple_display_type,
		$apple_legacy_active,
		$apple_hd_active,
		$metro_tile_color,
		$metro_hd_icon_active,
		$metro_wide_icon_active,
		$metro_wide_icon_image,
		$metro_html_tile_active,
		$browserconfig_active,
		$chrome_webmanifest_active,
		$chrome_display_type,
		$chrome_orientation,
		$chrome_bg_color,
		$chrome_color;


	/**
	 * Default Icon sizes
	 *
	 * @since 1.0.2
	 */
	protected $default_sd_icons_list = array(
		'16x16' => array(16, 16),
		'48x48' => array(48, 48),
		'32x32' => array(32, 32) // 32x32 must be always the last one
	);
	protected $default_hd_icons_list = array(
		'96x96' => array(96, 96),
		'128x128' => array(128, 128),
		'196x196' => array(196, 196)
	);


	/**
	 * Apple Icon sizes
	 *
	 * @version 1.0.6
	 * @since 1.0.2
	 * @link https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html
	 * @link https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/pinnedTabs/pinnedTabs.html
	 */
	protected $apple_legacy_icons_list = array(
		'40x40' => array(40, 40),
		'58x58' => array(58, 58),
		'60x60' => array(60, 60),
		'76x76' => array(76, 76),
		'80x80' => array(80, 80),
		'87x87' => array(87, 87),
		'120x120' => array(120, 120),
		'144x144' => array(144, 144)
	);
	protected $apple_touch_icons_list = array(
		'57x57' => array(57, 57),
		'72x72' => array(72, 72),
		'114x114' => array(114, 114),
		'152x152' => array(152, 152),
		'167x167' => array(167, 167),
		'180x180' => array(180, 180)
	);
	protected $apple_hd_icons_list = array(
		'1024x1024' => array(1024, 1024)
	);


	/**
	 * Apple Display Type values
	 *
	 * @version 1.0.8
	 * @since 1.0.7
	 */
	protected $apple_display_type_values = array(
		'1' => 'browser',
		'2' => 'standalone'
	);


	/**
	 * Metro sizes
	 *
	 * @since 1.0.2
	 * @link https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/dn320426(v=vs.85)/
	 */
	protected $metro_sd_square_icons_list = array(
		'70x70'   => array(70, 70),
		'150x150' => array(150, 150),
		'310x310' => array(310, 310)
	);
	protected $metro_sd_wide_icons_list = array(
		'310x150' => array(310, 150)
	);
	protected $metro_hd_square_icons_list = array(
		'70x70'   => array(128, 128),
		'150x150' => array(270, 270),
		'310x310' => array(558, 558)
	);
	protected $metro_hd_wide_icons_list = array(
		'310x150' => array(558, 270)
	);


	/**
	 * Chrome Desktop Icon sizes
	 *
	 * @since 1.0.2
	 * @link https://developers.google.com/web/updates/2014/11/Support-for-theme-color-in-Chrome-39-for-Android
	 */
	protected $chrome_desktop_icons_list = array(
		'192x192' => array(192, 192)
	);


	/**
	 * Chrome Mobile Icon sizes
	 *
	 * @version 1.0.6
	 * @since 1.0.3
	 * @link https://developers.google.com/web/fundamentals/web-app-manifest/?hl=es
	 */
	protected $chrome_mobile_icons_list = array(
		'128x128' => array(128, 128),
		'144x144' => array(144, 144),
		'152x152' => array(152, 152),
		'192x192' => array(192, 192),
		'256x256' => array(256, 256)
	);


	/**
	 * Chrome Orientation values
	 *
	 * @since 1.0.8
	 */
	protected $chrome_orientation_values = array(
		'1' => 'unset',
		'2' => 'portrait',
		'3' => 'landscape'
	);


	/**
	 * Chrome Display Type values
	 *
	 * @since 1.0.8
	 */
	protected $chrome_display_type_values = array(
		'1' => 'browser',
		'2' => 'standalone'
	);


	/**
	 * Set up hooks
	 *
	 * @version 1.1.2
	 * @since 1.0.2
	 */
	public function __construct()
	{
		// Remove default and print extended icons
		remove_action('wp_head', 'wp_site_icon', 100);
		add_action( 'wp_head', array ( $this, 'print_all'), 100 );

		// Register service worker
		wp_register_script('hbalm-service-worker', '/wp-content/plugins/' . HBALM_RELPATH . '/assets/js/service-worker.js', false, $this->plugin_version, true);

		self::set_active_sizes();
		self::set_file_locations();
	}


	/**
	 * Debug values
	 *
	 * @since 1.0.2
	 * @param $value (mixed)
	 * @param $exit  (boolean)
	 */
	public function _debug( $value, $exit = false )
	{
		echo '<pre>';
		var_dump($value);
		echo '</pre><hr />';

		if ($exit) exit();
	}


	/**
	 * File creater
	 *
	 * @since 1.0.8
	 * @param $file_path (string)
	 * @return (boolean)
	 */
	protected function file_creater( $file_path )
	{
		if ( file_exists($file_path) )
		{
			return true;
		}

		$new_file = @fopen($file_path, 'w');
		if ( $new_file == false )
		{
			//! Would be nice an attempt to create it using WP file permissions
			return false;
		}
		fputs($new_file, 1);
		fclose($new_file);


		return true;
	}


	/**
	 * File reader
	 *
	 * @version 1.0.8
	 * @since 1.0.3
	 * @param $file_path (string)
	 * @return (string)
	 */
	protected function file_reader( $file_path )
	{
		if ( !file_exists($file_path) )
		{
			if ( false === self::file_creater($file_path) )
			{
				return false;
			}
		}


		return file_get_contents($file_path);
	}


	/**
	 * File writer
	 *
	 * @version 1.0.8
	 * @since 1.0.3
	 * @param $file_path (string)
	 * @param $content   (string)
	 * @return (boolean)
	 */
	protected function file_writer( $file_path, $content = '' )
	{
		if ( !file_exists($file_path) )
		{
			if ( false === self::file_creater($file_path) )
			{
				return false;
			}
		}

		if ( false === @file_put_contents($file_path, $content) )
		{
			return false;
		}


		return true;
	}


	/**
	 * Set up file & folder locations
	 *
	 * @since 1.0.8
	 */
	protected function set_file_locations()
	{
		$path = HBALM_ABSPATH;

		if ( strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' )
		{
			//! Tested under Windows XAMPP
			$path = str_replace('/', '\\', HBALM_ABSPATH);
		}

		$this->webmanifest_fullpath = $path . 'site.webmanifest';
		$this->browserconfig_fullpath = $path . 'browserconfig.xml';
	}


	/**
	 * Merge all selected icon sizes
	 *
	 * @version 1.0.6
	 * @since 1.0.2
	 */
	protected function set_active_sizes()
	{
		self::fetch_data();

		// default
		$this->default_icons_sizes = array_merge(
			( $this->default_hd_active ? $this->default_hd_icons_list : array() ),
			$this->default_sd_icons_list
		);


		// Apple
		$this->apple_icons_sizes = array_merge(
			$this->apple_touch_icons_list,
			( $this->apple_legacy_active ? $this->apple_legacy_icons_list : array() ),
			( $this->apple_hd_active ? $this->apple_hd_icons_list : array() )
		);


		// Metro
		$this->metro_hd_icons_sizes = array_merge(
			$this->metro_hd_square_icons_list,
			( $this->metro_wide_icon_active ? $this->metro_hd_wide_icons_list : array() )
		);
		$this->metro_sd_icons_sizes = array_merge(
			$this->metro_sd_square_icons_list,
			( $this->metro_wide_icon_active ? $this->metro_sd_wide_icons_list : array() )
		);
		$this->metro_icons_sizes = ( $this->metro_hd_icon_active ? $this->metro_hd_icons_sizes : $this->metro_sd_icons_sizes );


		// Chrome
		$this->chrome_icons_sizes = $this->chrome_desktop_icons_list;


		// site.webmanifest file
		$this->webmanifest_icons_sizes = array_merge(
			$this->default_icons_sizes,
			$this->chrome_mobile_icons_list
		);
	}


	/**
	 * Fetch all data from database
	 *
	 * @version 1.0.10
	 * @since 1.0.2
	 */
	protected function fetch_data()
	{
		$base_settings   = get_option( 'hbalm_base_settings' );
		$apple_settings  = get_option( 'hbalm_apple_settings' );
		$metro_settings  = get_option( 'hbalm_metro_settings' );
		$chrome_settings = get_option( 'hbalm_chrome_settings' );

		// $data will be an array
		$data = array_merge(
			get_option( 'hbalm_base_settings'),
			get_option( 'hbalm_apple_settings'),
			get_option( 'hbalm_metro_settings'),
			get_option( 'hbalm_chrome_settings')
		);
		// self::_debug($data,1);

		foreach ( $data as $name => $value )
		{
			$this->{$name} = $value;
		}

		self::set_image();
	}


	/**
	 * Saves values in site.webmanifest file
	 *
	 * @version 1.1.2
	 * @since 1.0.3
	 * @param $valid_fields (array) - Validated form values
	 * @return (boolean)
	 */
	protected function save_webmanifest_file( $valid_fields = array() )
	{
		$manifest = self::retrieve_webmanifest();
		if ( !$manifest )
		{
			return false;
		}


		if ( isset($valid_fields['default_webmanifest_active']) )
		{
			if ( $valid_fields['default_webmanifest_active'] == true )
			{
				$manifest = self::default_save_webmanifest($manifest, $valid_fields);
			}
			else
			{
				$manifest = self::default_remove_webmanifest($manifest);
			}
		}


		if ( isset($valid_fields['chrome_webmanifest_active']) )
		{
			if ( $valid_fields['chrome_webmanifest_active'] == true )
			{
				$manifest = self::chrome_save_webmanifest($manifest, $valid_fields);
			}
			else
			{
				$manifest = self::chrome_remove_webmanifest($manifest);
			}
		}

		return self::file_writer($this->webmanifest_fullpath, stripslashes(json_encode($manifest)));
	}


	/**
	 * Open & reads site.webmanifest
	 *
	 * @version 1.1.2
	 * @since 1.0.6
	 * @return (array) JSON decoded webmanifest content
	 */
	private function retrieve_webmanifest()
	{
		$file_content = self::file_reader($this->webmanifest_fullpath);
		if ( false === $file_content )
		{
			// Unable to read/ create file
			return false;
		}

		$manifest_content = json_decode($file_content, TRUE);
		if ( is_null($manifest_content)
			|| !is_array($manifest_content)
			|| !isset($manifest_content['name'])
			|| !isset($manifest_content['start_url'])
			|| !isset($manifest_content['icons']) )
		{
			// File corrupted
			return self::prepare_webmanifest();
		}


		return self::prepare_webmanifest( $manifest_content );
	}


	/**
	 * Adds the common site.webmanifest values
	 *
	 * @version 1.0.7
	 * @since 1.0.6
	 * @param $manifest (array)
	 * @return (array)
	 */
	private function prepare_webmanifest( $manifest = false )
	{
		if ( !$manifest
			|| !is_array($manifest) )
		{
			$manifest = array();
		}


		// Common values
		$manifest['name']       = get_bloginfo( 'name' );
		$manifest['short_name'] = get_bloginfo( 'name' );
		$manifest['start_url']  = get_bloginfo( 'url' );
		$manifest['icons']      = array();

		foreach ( $this->webmanifest_icons_sizes as $size => $webmanifest_icon )
		{
			$manifest_icon_href = self::rw_image_resize( $this->image_id , $webmanifest_icon[0], $webmanifest_icon[1], true );

			$manifest['icons'][] = array(
				"src"   => $manifest_icon_href,
				"sizes" => $size,
				"type"  => "image/png"
			);
		}


		return $manifest;
	}


	/**
	 * Saves default values in site.webmanifest file
	 *
	 * @link https://developer.mozilla.org/es/docs/Web/Manifest
	 * @version 1.1.2
	 * @since 1.0.6
	 * @param $manifest (array)
	 * @param $valid_fields (array)
	 * @return (array)
	 */
	private function default_save_webmanifest( $manifest, $valid_fields )
	{

		// Default values
		$manifest['background_color']     = $valid_fields['default_bg_color'];
		$manifest['description']          = get_bloginfo( 'description' );
		$manifest['dir']                  = 'ltr';
		$manifest['lang']                 = get_bloginfo( 'language' );
		$manifest['related_applications'] = array(
			array(
				'platform' => 'web',
				'url' => get_bloginfo( 'url' ),
			)
		);
		if ( function_exists( 'is_rtl' ) && is_rtl() )
		{
			$manifest['dir'] = 'rtl';
		}


		return $manifest;
	}


	/**
	 * Removes default values from site.webmanifest file
	 *
	 * @since 1.0.6
	 * @param $manifest (array)
	 * @return (array)
	 */
	private function default_remove_webmanifest( $manifest )
	{
		unset(
			$manifest['background_color'],
			$manifest['description'],
			$manifest['dir'],
			$manifest['lang'],
			$manifest['related_applications'],
			$manifest['dir']
		);


		return $manifest;
	}


	/**
	 * Saves Chrome values in site.webmanifest file
	 *
	 * @link https://developers.google.com/web/fundamentals/web-app-manifest/
	 * @version 1.1.2
	 * @since 1.0.6
	 * @param $manifest (array)
	 * @param $valid_fields (array)
	 * @return (array)
	 */
	private function chrome_save_webmanifest( $manifest, $valid_fields )
	{
		$ThemeColor = $valid_fields['chrome_color'] == '' ? $this->default_bg_color : $valid_fields['chrome_color'];
		$BackgroundColor = $valid_fields['chrome_bg_color'] == '' ? $this->default_bg_color : $valid_fields['chrome_bg_color'];

		$manifest['theme_color']      = $ThemeColor;
		$manifest['background_color'] = $BackgroundColor;
		$manifest['display']          = isset($this->chrome_display_type_values[$valid_fields['chrome_display_type']]) ? $this->chrome_display_type_values[$valid_fields['chrome_display_type']] : 'standalone';


		return $manifest;
	}


	/**
	 * Removes Chrome values from site.webmanifest file
	 *
	 * @since 1.0.6
	 * @param $manifest (array)
	 * @return (array)
	 */
	private function chrome_remove_webmanifest( $manifest )
	{
		unset(
			$manifest['theme_color'],
			$manifest['background_color'],
			$manifest['display']
		);


		return $manifest;
	}


	/**
	 * Saves values in browserconfig.xml file
	 *
	 * @version 1.0.8
	 * @since 1.0.2
	 * @return (boolean)
	 */
	protected function save_browserconfig_file()
	{
		$browserconfig = self::retrieve_browserconfig();
		if ( false === $browserconfig )
		{
			// Unable to read/ create file
			return false;
		}

		$TileColor = $this->metro_tile_color == '' ? $this->default_bg_color : $this->metro_tile_color;

		// Tile
		unset($browserconfig->msapplication->tile);
		$browserconfig_tile = $browserconfig->msapplication->addChild('tile');
		$browserconfig_tile->addChild('TileColor', $TileColor);

		// Icons
		foreach ( $this->metro_icons_sizes as $sizes => $metro_icon )
		{
			if ( $metro_icon[0] != $metro_icon[1] )
			{
				// Non-square size
				if ( !empty($this->metro_wide_icon_image) )
				{
					$wide_icon_id = self::get_attachment_id($this->metro_wide_icon_image);
					$wide_icon_href = self::rw_image_resize( $wide_icon_id , $metro_icon[0], $metro_icon[1], true );

					if ( !$browserconfig_tile->{'wide' . $sizes . 'logo'} )
					{
						$browserconfig_tile->addChild('wide' . $sizes . 'logo');
					}
					$browserconfig_tile->{'wide' . $sizes . 'logo'}->addAttribute('src', $wide_icon_href);
				}
				continue;
			}

			$default_icon_href = self::rw_image_resize( $this->image_id , $metro_icon[0], $metro_icon[1], true );

			if ( !$browserconfig_tile->{'square' . $sizes . 'logo'} )
			{
				$browserconfig_tile->addChild('square' . $sizes . 'logo');
			}
			$browserconfig_tile->{'square' . $sizes . 'logo'}->addAttribute('src', $default_icon_href);
		}


		return self::file_writer($this->browserconfig_fullpath, $browserconfig->asXML());
	}


	/**
	 * Open & reads site.webmanifest
	 *
	 * @since 1.0.8
	 * @return (array) JSON decoded webmanifest content
	 */
	private function retrieve_browserconfig()
	{
		$file_content = self::file_reader($this->browserconfig_fullpath);
		if ( false === $file_content )
		{
			return false;
		}

		libxml_use_internal_errors(true);
		$browserconfig_content = simplexml_load_string($file_content);
		if ( false === $browserconfig_content )
		{
			// File corrupted
			$browserconfig_content = simplexml_load_string('<?xml version="1.0" encoding="utf-8"?><browserconfig><msapplication></msapplication></browserconfig>');
		}


		return $browserconfig_content;
	}


	/**
	 * Sets $this->image_id
	 *
	 * @since 1.0.0
	 * @return (int) $this->image_id
	 */
	private function set_image()
	{
		$site_icon = get_option( 'site_icon' );
		if ( $site_icon && $site_icon != 0 )
		{
			$this->image_id = $site_icon;
		}
		elseif ( has_custom_logo() )
		{
			$this->image_id = get_theme_mod( 'custom_logo' );
		}
		else
		{
			return false;
		}
	}


	/**
	 * Retrieves attachment_id from DDBB using URL
	 *
	 * @since 1.0.2
	 * @param (string) Attachment URL
	 * @return (int) Attachment Id
	 */
	public function get_attachment_id( $attachment_url )
	{
		global $wpdb;

		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid = '%s';", $attachment_url ));

		return $attachment[0];
	}


	/**
	 * Create new image size if not exists
	 *
	 * @since 1.0.0
	 * @link https://deluxeblogtips.com/resize-image-fly-wordpress/
	 * @return (string) Media URL
	 */
	private function rw_image_resize( $attachment_id, $width, $height, $crop = true )
	{
		$path = get_attached_file( $attachment_id );
		if ( !file_exists( $path ) ) return false;


		$upload = wp_upload_dir();
		$path_info = pathinfo( $path );
		$base_url = $upload['baseurl'] . str_replace( $upload['basedir'], '', $path_info['dirname'] );

		$meta = wp_get_attachment_metadata( $attachment_id );
		foreach ( $meta['sizes'] as $key => $size )
		{
			if ( $size['width'] == $width && $size['height'] == $height )
			{
				return "{$base_url}/{$size['file']}";
			}
		}

		// Generate new size
		$resized = image_make_intermediate_size( $path, $width, $height, $crop );
		if ( $resized && ! is_wp_error( $resized ) )
		{
			// Let metadata know about our new size.
			$key                 = sprintf( 'resized-%dx%d', $width, $height );
			$meta['sizes'][$key] = $resized;
			wp_update_attachment_metadata( $attachment_id, $meta );
			return "{$base_url}/{$resized['file']}";
		}

		// Return original if fails
		return "{$base_url}/{$path_info['basename']}";
	}


	/**
	 * Creates icon size link & meta head tags
	 *
	 * @version 1.0.2
	 * @since 1.0.0
	 * @return (string) HTML
	 */
	public function get_all()
	{
		self::fetch_data();

		$html_string  = self::get_default_icons();
		$html_string .= self::get_apple_headers();
		$html_string .= self::get_metro_headers();
		$html_string .= self::get_chrome_headers();

		return $html_string;
	}


	/**
	 * Print all tags
	 *
	 * @version 1.1.2
	 * @since 1.0.2
	 */
	public function print_all()
	{
		echo '<!-- START App icon generator -->' . PHP_EOL;
		self::print_service_worker();
		echo self::get_all();
		echo '<!-- END App icon generator -->' . PHP_EOL;
	}


	/**
	 * Get $this->image_id and $this->default_icons sizes and prints default favicon <link> icons
	 *
	 * @version 1.0.2
	 * @since 1.0.0
	 * @return (string) HTML
	 */
	protected function get_default_icons()
	{
		if ( !$this->image_id ) return '';

		$html_string = '';
		foreach ( $this->default_icons_sizes as $sizes => $default_icon )
		{
			$default_icon_href = self::rw_image_resize( $this->image_id , $default_icon[0], $default_icon[1], true );

			$html_string .= '<link rel="shortcut icon" type="image/png" sizes="' . $sizes . '" href="' . $default_icon_href . '?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}

		return $html_string;
	}


	/**
	 * Returns all Apple <meta> and <link> tags
	 *
	 * @since 1.0.2
	 * @return (string) HTML
	 */
	protected function get_apple_headers()
	{
		$html_string  = $this->get_apple_metas();
		$html_string .= $this->get_apple_icons();

		return $html_string;
	}


	/**
	 * Returns apple-mobile-web-app-title <meta> tags
	 *
	 * @version 1.0.1
	 * @since 1.0.0
	 * @return (string) HTML
	 */
	protected function get_apple_metas()
	{
		$html_string = '<meta name="apple-mobile-web-app-title" content="' . get_bloginfo( 'name' ) . '" />' . PHP_EOL;

		if ( $this->apple_display_type == 2 )
		{
			$html_string .= '<meta name="apple-mobile-web-app-capable" content="yes">' . PHP_EOL;
		}


		return $html_string;
	}


	/**
	 * Uses $this->image_id and $this->apple_icons to return all Apple <link> icons
	 *
	 * @version 1.0.6
	 * @since 1.0.0
	 * @return (string) HTML
	 */
	protected function get_apple_icons()
	{
		if ( !$this->image_id ) return '';

		$html_string = '';
		foreach ( $this->apple_icons_sizes as $sizes => $apple_icon )
		{			
			$default_icon_href = self::rw_image_resize( $this->image_id, $apple_icon[0], $apple_icon[1], true );
			
			$html_string .= '<link rel="apple-touch-icon-precomposed apple-touch-icon" sizes="' . $sizes . '" href="' . $default_icon_href . '?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}

		return $html_string;
	}


	/**
	 * Returns all Metro <meta> tags
	 *
	 * @since 1.0.2
	 * @return (string) HTML
	 */
	protected function get_metro_headers()
	{
		$html_string  = $this->get_metro_metas();
		$html_string .= $this->get_metro_icons();

		return $html_string;
	}


	/**
	 * Returns Metro application-name, msapplication-TileColor and browserconfig.xml (if active) metas
	 *
	 * @version 1.1.2
	 * @since 1.0.2
	 * @return (string) HTML
	 */
	protected function get_metro_metas()
	{
		$TileColor = $this->metro_tile_color == '' ? $this->default_bg_color : $this->metro_tile_color;

		$html_string = '<meta name="application-name" content="' . get_bloginfo( 'name' ) . '" />' . PHP_EOL;
		$html_string .= '<meta name="msapplication-TileColor" content="' . $TileColor . '" />' . PHP_EOL;

		if ( $this->browserconfig_active )
		{
			$html_string .= '<meta name="msapplication-config" content="/wp-content/plugins/' . HBALM_RELPATH . '/browserconfig.xml?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}

		return $html_string;
	}


	/**
	 * Uses $this->image_id and $this->metro_icons_sizes to return all Metro <meta> icons
	 *
	 * @version 1.0.3
	 * @since 1.0.0
	 * @return (string) HTML
	 */
	protected function get_metro_icons()
	{
		if ( !$this->image_id ) return '';


		$html_string = '';
		if ( $this->metro_html_tile_active )
		{
			$tile_image_href = self::rw_image_resize( $this->image_id , 144, 144, true );

			$html_string .= '<meta name="msapplication-TileImage" content="' . $tile_image_href . '?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}

		foreach ( $this->metro_icons_sizes as $sizes => $metro_icon )
		{
			if ( $metro_icon[0] != $metro_icon[1] )
			{
				// Non-square size
				if ( !empty($this->metro_wide_icon_image) )
				{
					$wide_icon_id = self::get_attachment_id($this->metro_wide_icon_image);
					$wide_icon_href = self::rw_image_resize( $wide_icon_id , $metro_icon[0], $metro_icon[1], true );
					$html_string .= '<meta name="msapplication-wide' . $sizes . 'logo" content="' . $wide_icon_href . '?v=' . $this->plugin_version . '" />' . PHP_EOL;
				}
				continue;
			}

			$metro_icon_href = self::rw_image_resize( $this->image_id , $metro_icon[0], $metro_icon[1], true );
			$html_string .= '<meta name="msapplication-square' . $sizes . 'logo" content="' . $metro_icon_href . '?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}

		return $html_string;
	}


	/**
	 * Returns all Chrome <meta> and <link> tags
	 *
	 * @since 1.0.2
	 * @return (string) HTML
	 */
	protected function get_chrome_headers()
	{
		$html_string = $this->get_chrome_metas();
		$html_string .= $this->get_chrome_icons();

		return $html_string;
	}


	/**
	 * Return Chrome theme-color meta
	 *
	 * @version 1.1.2
	 * @since 1.0.0
	 * @return (string) HTML
	 */
	protected function get_chrome_metas()
	{
		$ThemeColor = $this->chrome_color == '' ? $this->default_bg_color : $this->chrome_color;

		$html_string = '<meta name="theme-color" content="' . $ThemeColor . '">' . PHP_EOL;
		if ( $this->chrome_webmanifest_active
			|| $this->default_webmanifest_active )
		{
			$html_string .= '<link rel="manifest" href="/wp-content/plugins/' . HBALM_RELPATH . '/site.webmanifest?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}


		return $html_string;
	}



	/**
	 * Prints Chrome service worker
	 *
	 * @since 1.1.2
	 */
	protected function print_service_worker()
	{
		?>
			<script type="text/javascript">
				if ( navigator.serviceWorker ) {
					navigator.serviceWorker.register('/wp-content/plugins/<?php echo HBALM_RELPATH; ?>/assets/js/service-worker.php', { scope: '/' }).then( function(registration) {
						console.log('ServiceWorker registration successful with scope: ',  registration.scope);
					}).catch( function(error ) {
						console.log('ServiceWorker registration failed: ', error);
					});
				}
			</script>
		<?php
	}


	/**
	 * Get $this->image_id and $this->chrome_icons_sizes prints Chrome color <meta> and <link> icon
	 *
	 * @since 1.0.2
	 * @return (string) HTML
	 */
	protected function get_chrome_icons()
	{
		if ( !$this->image_id ) return '';
		
		$html_string = '';
		foreach ( $this->chrome_icons_sizes as $sizes => $chrome_icon )
		{
			$default_icon_href = self::rw_image_resize( $this->image_id , $chrome_icon[0], $chrome_icon[1], true );
			$html_string .= '<link rel="icon" sizes="' . $sizes . '" href="' . $default_icon_href . '?v=' . $this->plugin_version . '" />' . PHP_EOL;
		}

		return $html_string;
	}
}



/**
 * WP Admin Pages - App icon generator
 *
 * @version 1.0.8
 * @since 1.0.3
 */
class Admin_Head_Browser_App_Links_Metas extends Head_Browser_App_Links_Metas
{
	// Admin success messages
	private $settings_success_msg_array = array();

	// Admin error messages
	private $settings_error_msg_array = array();


	/**
	 * Set up admin hooks
	 *
	 * @version 1.0.10
	 * @since 1.0.3
	 */
	public function __construct()
	{
		parent::__construct();

		add_action( 'admin_enqueue_scripts', array(&$this, 'admin_page_scripts') );

		add_filter( 'all_plugins', array( $this, 'translate_description_at_plugins_page' ) );
		add_filter( 'plugin_action_links_' . HBALM_BASENAME, array( $this, 'settings_link_at_plugins_page' ) );

		add_action( 'admin_menu', array( $this, 'add_admin_sidebar_menu' ) );
		// add_action( 'admin_init', array( $this, 'add_admin_filters') );

		add_action( 'admin_init', array( $this, 'add_base_setting' ) );
		add_action( 'admin_init', array( $this, 'add_apple_settings' ) );
		// add_action( 'admin_init', array( $this, 'add_metro_settings' ) );
		register_setting( 'hbalm_metro',  'hbalm_metro_settings',  array( $this, 'validate_metro_options' ) );
		// add_action( 'admin_init', array( $this, 'add_chrome_settings' ) );
		register_setting( 'hbalm_chrome', 'hbalm_chrome_settings', array( $this, 'validate_chrome_options' ) );

		add_action( 'admin_notices', array( $this, 'show_admin_notices' ) );

		load_plugin_textdomain( 'hbalm', false, HBALM_RELPATH . '/languages' );
	}


	/**
	 * Plugin activation
	 *
	 * @since 1.0.10
	 */
	public function hbalm_activation()
	{
		$base_settings   = get_option( 'hbalm_base_settings' );
		$apple_settings  = get_option( 'hbalm_apple_settings' );
		$metro_settings  = get_option( 'hbalm_metro_settings' );
		$chrome_settings = get_option( 'hbalm_chrome_settings' );

		// General
		if ( !$base_settings )
		{
			$base_settings = array(
				'default_bg_color'           => '#fafafa',
				'default_hd_active'          => false,
				'default_webmanifest_active' => false,
			);
		}

		// Apple
		if ( !$apple_settings )
		{
			$apple_settings = array(
				'apple_bg_color'      => '',
				'apple_display_type'  => 2,
				'apple_legacy_active' => false,
				'apple_hd_active'     => false,
			);
		}

		// Metro
		if ( !$metro_settings )
		{
			$metro_settings = array(
				'metro_tile_color'       => '',
				'metro_hd_icon_active'   => false,
				'metro_html_tile_active' => false,
				'browserconfig_active'   => false,
				'metro_wide_icon_active' => false,
				'metro_wide_icon_image'  => '',
			);
		}

		// Chrome
		if ( !$chrome_settings )
		{
			$chrome_settings = array(
				'chrome_webmanifest_active' => false,
				'chrome_bg_color'           => '',
				'chrome_color'              => '',
				'chrome_display_type'       => 2,
				'chrome_orientation'        => 1,
			);
		}

		add_option( 'hbalm_base_settings',   $base_settings );
		add_option( 'hbalm_apple_settings',  $apple_settings );
		add_option( 'hbalm_metro_settings',  $metro_settings );
		add_option( 'hbalm_chrome_settings', $chrome_settings );
	}


	/**
	 * Plugin uninstall
	 *
	 * @since 1.0.10
	 */
	public function hblam_uninstall()
	{
		// Delete stored option values
		delete_option( 'hbalm_base_settings' );
		delete_option( 'hbalm_apple_settings' );
		delete_option( 'hbalm_metro_settings' );
		delete_option( 'hbalm_chrome_settings' );

		// Unregister settings
		unregister_setting( 'hbalm_page',   'hbalm_base_settings' );
		unregister_setting( 'hbalm_apple',  'hbalm_apple_settings' );
		unregister_setting( 'hbalm_metro',  'hbalm_metro_settings' );
		unregister_setting( 'hbalm_chrome', 'hbalm_chrome_settings' );
	}


	/**
	 * Show admin errors
	 *
	 * @since 1.0.7
	 */
	public function show_admin_notices()
	{
		settings_errors( 'hbalm_settings' );
	}


	/**
	 * Stores all individual page success messages and removes error message
	 *
	 * @since 1.0.7
	 * @param $field_name      (string)
	 * @param $success_message (string)
	 */
	private function enqueue_success_msg( $field_name, $success_message = '' )
	{
		unset($this->settings_error_msg_array[$field_name]);

		$this->settings_success_msg_array[$field_name] = $success_message != '' ? $success_message : sprintf('%s %s', $field_name, __('updated', 'hbalm'));
	}


	/**
	 * Sends admin success message
	 *
	 * @since 1.0.7
	 */
	private function add_hbalm_success()
	{
		if ( !is_array($this->settings_success_msg_array) ) return false;
		if ( count($this->settings_success_msg_array) <= 0 )  return false;

		$full_msg = '';
		foreach ( $this->settings_success_msg_array as $field_name => $success_message )
		{
			$full_msg .= sprintf('%s<br />', $success_message);
		}
		add_settings_error( 'hbalm_settings', 'hbalm_success', $full_msg, 'updated' );
	}


	/**
	 * Stores admin error messages and removes success message
	 *
	 * @since 1.0.7
	 * @param $field_name    (string)
	 * @param $error_message (string)
	 */
	private function enqueue_settings_error( $field_name, $error_message = '' )
	{
		unset($this->settings_success_msg_array[$field_name]);

		$this->settings_error_msg_array[$field_name] = sprintf('%s %s', $field_name, ($error_message != '' ? $error_message : __('was not updated', 'hbalm')));
	}


	/**
	 * Sends admin success message
	 *
	 * @since 1.0.7
	 */
	private function add_hbalm_error()
	{
		if ( !is_array($this->settings_error_msg_array) ) return false;
		if ( count($this->settings_error_msg_array) <= 0 )  return false;

		$full_msg = '';
		foreach ( $this->settings_error_msg_array as $field_name => $text )
		{
			$full_msg .= $text;
		}
		add_settings_error( 'hbalm_settings', 'hbalm_success', $full_msg, 'error' );
	}


	/**
	 * Sends admin success message
	 *
	 * @since 1.0.7
	 */
	private function add_settings_messages()
	{
		self::add_hbalm_error();
		self::add_hbalm_success();
	}


	/**
	 * Admin page CSS & JS scripts
	 *
	 * @version 1.0.10
	 * @since 1.0.2
	 */
	public function admin_page_scripts()
	{
		// Admin layout
		wp_enqueue_style( 'hbalm-layout', HBALM_URL . 'assets/css/layout.min.css', array(), $this->plugin_version );
		// Admin theme
		wp_enqueue_style( 'hbalm-theme', HBALM_URL . 'assets/css/theme.min.css', array(), $this->plugin_version );

		// Color Picker
		wp_enqueue_style( 'wp-color-picker' );

		// Image upload & crop
		wp_enqueue_media();
		wp_register_script('hblam-image-cropper', HBALM_URL . 'assets/js/hblam-image-cropper.min.js', array( 'jquery' ), $this->plugin_version, true);
		wp_enqueue_script('hblam-image-cropper');

		// Plugin scripts
		wp_register_script('hbalm-script', HBALM_URL . 'assets/js/hbalm-script.min.js', array( 'jquery', 'wp-color-picker' ), $this->plugin_version, true);
		wp_enqueue_script('hbalm-script');

		// Tooltips
		wp_enqueue_style( 'hbalm-tooltips', HBALM_URL . 'assets/css/tooltips.min.css', array(), $this->plugin_version );

		// Media upload
		wp_enqueue_style('thickbox');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
	}


	/**
	 * General admin page cb
	 *
	 * @since 1.0.4
	 */
	public function general_admin_form()
	{
		$this->settings = 'hbalm_page';
		self::base_admin_page_design();
	}


	/**
	 * General admin page sidebar
	 *
	 * @version 1.0.10
	 * @since 1.0.4
	 */
	public function hbalm_page_form_help()
	{
		?>
			<div>
				<?php _e('You can find more info about webmanifest', 'hbalm'); ?>
				<?php printf( '<a href="%s" target="_blank">%s</a>', 'https://developer.mozilla.org/en-US/docs/Web/Manifest', __('here', 'hbalm') ); ?>
			</div>
		<?php
	}


	/**
	 * Apple admin page cb
	 *
	 * @since 1.0.4
	 */
	public function apple_admin_form()
	{
		$this->settings = 'hbalm_apple';
		self::base_admin_page_design();
	}


	/**
	 * Apple admin page sidebar
	 *
	 * @version 1.0.10
	 * @since 1.0.4
	 */
	public function hbalm_apple_form_help()
	{
		?>
			<div>
				<?php _e('You can find more info about Apple web applications', 'hbalm'); ?>
				<?php printf( '<a href="%s" target="_blank">%s</a>', 'https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html', __('here', 'hbalm') ); ?>
			</div>
		<?php
	}


	/**
	 * Metro admin page cb
	 *
	 * @version 1.0.9
	 * @since 1.0.5
	 */
	public function metro_admin_form()
	{
		parent::fetch_data();

		if ( !defined('HBALM_ADMIN') )
		{
			define('HBALM_ADMIN', true);
		}
		include_once( HBALM_ABSPATH . 'admin/metro.php' );
	}


	/**
	 * Metro admin page sidebar
	 *
	 * @version 1.0.10
	 * @since 1.0.5
	 * @see self::metro_admin_form()
	 */
	public function hbalm_metro_form_help()
	{
		?>
			<div>
				<?php _e('You can find more info about Metro tiles', 'hbalm'); ?>
				<?php printf( '<a href="%s" target="_blank">%s</a>', 'https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/dn320426(v=vs.85)', __('here', 'hbalm') ); ?>
			</div>
		<?php
	}


	/**
	 * Chrome admin page cb
	 *
	 * @version 1.0.10
	 * @since 1.0.5
	 */
	public function chrome_admin_form()
	{
		parent::fetch_data();

		if ( !defined('HBALM_ADMIN') )
		{
			define('HBALM_ADMIN', true);
		}
		include_once( HBALM_ABSPATH . 'admin/chrome.php' );
	}


	/**
	 * Chrome admin page sidebar
	 *
	 * @version 1.0.10
	 * @since 1.0.5
	 * @see self::chrome_admin_form()
	 */
	public function hbalm_chrome_form_help()
	{
		?>
			<div>
				<?php _e('You can find more info about Chrome webmanifest', 'hbalm'); ?>
				<?php printf( '<a href="%s" target="_blank">%s</a>', 'https://developers.google.com/web/fundamentals/web-app-manifest/?hl=es', __('here', 'hbalm') ); ?>
			</div>
		<?php
	}


	/**
	 * Admin page base design
	 *
	 * @version 1.0.6
	 * @since 1.0.2
	 */
	private function base_admin_page_design()
	{
		?>
			<div class="wrap">
				<h1><?php _e( 'App icon generator Settings', 'hbalm' ); ?></h1>
				<hr />
				<div id="hbalm-main">
					<form action="options.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
						<?php settings_fields($this->settings); ?>
						<?php do_settings_sections($this->settings); ?>
						<?php wp_nonce_field('update_settings', 'hbalm_nonce'); ?>
						<?php submit_button(); ?>
					</form>
				</div>
				<?php $this->print_sidebar(); ?>
			</div>
		<?php
	}


	/**
	 * Prints admin page help sidebar
	 *
	 * @param $section (string) - Name of the help section
	 * @since 1.0.10
	 */
	private function print_sidebar( $section = false )
	{
		if ( !$section )
		{
			$section = $this->settings;
		}

		if ( method_exists($this, $section.'_form_help') )
		{
			?>
				<div id="hbalm-sidebar">
					<h2> <?php _e('Help', 'hbalm'); ?> </h2>
					<?php $this->{$section.'_form_help'}(); ?>
				</div>
			<?php
		}
	}


	/**
	 * Add Settings option inside WP Installed Plugins Page links
	 *
	 * @since 1.0.2
	 */
	public function settings_link_at_plugins_page( $data )
	{
		if ( !current_user_can('manage_options') ) return $data;

		return array_merge(
			$data,
			array(
				sprintf(
					'<a href="%s">%s</a>',
					add_query_arg(
						array(
							'page' => 'hbalm_page'
						),
						admin_url('admin.php')
					),
					__("Settings", "hbalm")
				)
			)
		);
	}


	/**
	 * Plugin description translation
	 *
	 * @version 1.0.10
	 * @since 1.0.6
	 */
	public function translate_description_at_plugins_page( $all_plugins )
	{
		if ( isset( $all_plugins[HBALM_BASENAME] ) )
		{
			$all_plugins[HBALM_BASENAME]['Description'] = __( 'Creates all size site icons for Chrome, iOS/Safari, IE/Edge/Metro and Firefox (among others) using WordPress logo image and sets up your site as a standalone mobile application', 'hbalm' );
		}


		return $all_plugins;
	}


	/**
	 * Adding admin menu link
	 *
	 * @version 1.0.6
	 * @since 1.0.2
	 */
	public function add_admin_sidebar_menu()
	{
		add_menu_page(
			__( 'App icon generator', 'hbalm' ), // page_title
			__( 'App icon generator', 'hbalm' ), // menu_title
			'manage_options', // capability
			'hbalm_page', // menu_slug
			'', // function
			'dashicons-tablet', // icon_url
			81 // position
		);

		add_submenu_page(
			'hbalm_page', // parent_slug
			__( 'General Settings', 'hbalm' ), // page_title
			__( 'General Settings', 'hbalm' ), // menu_title
			'manage_options', // capability
			'hbalm_page', // menu_slug
			array( $this, 'general_admin_form' ) // function
		);

		add_submenu_page(
			'hbalm_page',
			__( 'Apple Settings', 'hbalm' ),
			__( 'Apple Settings', 'hbalm' ),
			'manage_options',
			'hbalm_apple',
			array( $this, 'apple_admin_form' )
		);

		add_submenu_page(
			'hbalm_page',
			__( 'Metro Settings', 'hbalm' ),
			__( 'Metro Settings', 'hbalm' ),
			'manage_options',
			'hbalm_metro',
			array( $this, 'metro_admin_form' )
		);

		add_submenu_page(
			'hbalm_page',
			__( 'Chrome Settings', 'hbalm' ),
			__( 'Chrome Settings', 'hbalm' ),
			'manage_options',
			'hbalm_chrome',
			array( $this, 'chrome_admin_form' )
		);
	}


	/**
	 * Register general admin page options
	 *
	 * @version 1.0.6
	 * @since 1.0.2
	 */
	public function add_base_setting()
	{
		parent::fetch_data();

		// Add Section for option fields
		add_settings_section( 'hblam_section', __( 'General settings', 'hbalm' ), array( $this, 'null_cb' ), 'hbalm_page' ); // id, title, display cb, page


		// Background Color Field
		add_settings_field( 'default_bg_color', __( 'Icons bg color', 'hbalm' ), array( $this, 'add_color_input' ), 'hbalm_page', 'hblam_section', array('section' => 'hbalm_base_settings', 'setting' => 'default_bg_color') ); // id, title, display cb, page, section, args

		// Default HD icons
		add_settings_field( 'default_hd_active', __( 'Default HD sizes', 'hbalm' ), array( $this, 'default_hd_active_input' ), 'hbalm_page', 'hblam_section' );

		// site.webmanifest file
		add_settings_field( 'default_webmanifest_active', __( 'Create default webmanifest file', 'hbalm' ), array( $this, 'default_webmanifest_active_input' ), 'hbalm_page', 'hblam_section' );


		// Register Settings
		register_setting( 'hbalm_page', 'hbalm_base_settings', array( $this, 'validate_base_options' ) ); // option group, option name, sanitize cb
	}


	/**
	 * Background Color Input creation
	 *
	 * Replaces all other individual color input functions
	 *
	 * @since 1.0.6
	 * @param $args (array)
	 * {
	 *     section => (string)
	 *     setting => (string) - Input name
	 * }
	 */
	public function add_color_input( $args )
	{
		if ( !isset($args['section'])
			|| !isset($args['setting']) )
		{
			return false;
		}

		?>
			<input type="text" name="<?php echo $args['section']; ?>[<?php echo $args['setting']; ?>]" value="<?php echo $this->{$args['setting']}; ?>" class="hbalm-colorpicker" />
		<?php
	}


	/**
	 * Main Background Color Input
	 *
	 * @version 1.0.4
	 * @since 1.0.2
	 * @deprecated in 1.0.6
	 * @see add_color_input()
	 */
	public function main_bg_color_input()
	{
		?>
			<input type="text" name="hbalm_base_settings[default_bg_color]" value="<?php echo $this->default_bg_color; ?>" class="hbalm-colorpicker" />
		<?php
	}


	/**
	 * Default HD sizes
	 *
	 * @version 1.0.10
	 * @since 1.0.2
	 */
	public function default_hd_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_base_settings[default_hd_active]" value="1" <?php checked( $this->default_hd_active ); ?> />
			<div class="tooltip">
				<span class="tooltiptext tooltip-right">
					<ul>
						<li><strong>HD</strong></li>
						<?php foreach ( $this->default_hd_icons_list as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
						<li><hr /></li>
						<li><strong>SD</strong></li>
						<?php foreach ( $this->default_sd_icons_list as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
					</ul>
				</span>	
			</div>
		<?php
	}


	/**
	 * Default webmanifest file values active input
	 *
	 * @since 1.0.6
	 */
	public function default_webmanifest_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_base_settings[default_webmanifest_active]" value="1" <?php checked( $this->default_webmanifest_active ); ?> />
		<?php
	}


	/**
	 * Register Apple admin page options
	 *
	 * @version 1.0.6
	 * @since 1.0.4
	 */
	public function add_apple_settings()
	{
		parent::fetch_data();

		// Add Section for option fields
		add_settings_section( 'hblam_apple_section', __( 'Apple settings', 'hbalm' ), array( $this, 'null_cb' ), 'hbalm_apple' ); // id, title, display cb, page


		// Apple Background Color Field
		add_settings_field( 'apple_bg_color', __( 'Apple icons bg color', 'hbalm' ), array( $this, 'add_color_input' ), 'hbalm_apple', 'hblam_apple_section', array('section' => 'hbalm_apple_settings', 'setting' => 'apple_bg_color') ); // id, title, display cb, page, section, args

		// Apple standalone app
		add_settings_field( 'apple_display_type', __( 'Apple app display type', 'hbalm' ), array( $this, 'apple_display_type_input' ), 'hbalm_apple', 'hblam_apple_section' );

		// Apple legacy icons
		add_settings_field( 'apple_legacy_active', __( 'Apple legacy sizes', 'hbalm' ), array( $this, 'apple_legacy_active_input' ), 'hbalm_apple', 'hblam_apple_section' );

		// Apple HD icons
		add_settings_field( 'apple_hd_active', __( 'Apple HD size', 'hbalm' ), array( $this, 'apple_hd_active_input' ), 'hbalm_apple', 'hblam_apple_section' );


		// Register Settings
		register_setting( 'hbalm_apple', 'hbalm_apple_settings', array( $this, 'validate_apple_options' ) ); // option group, option name, sanitize cb
	}


	/**
	 * Apple display type select field
	 *
	 * @version 1.0.7
	 * @since 1.0.6
	 */
	public function apple_display_type_input()
	{
		?>
			<select name="hbalm_apple_settings[apple_display_type]">
				<?php foreach ( $this->apple_display_type_values as $value => $name ): ?>
					<option value="<?php echo $value; ?>" <?php selected( $this->apple_display_type, $value ); ?>><?php echo $name; ?></option>
				<?php endforeach; ?>
			</select>
		<?php
	}


	/**
	 * Apple legacy sizes
	 *
	 * @version 1.0.4
	 * @since 1.0.2
	 */
	public function apple_legacy_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_apple_settings[apple_legacy_active]" value="1" <?php checked( $this->apple_legacy_active ); ?> />
			<div class="tooltip">
				<span class="tooltiptext tooltip-right">
					<ul>
						<?php foreach ( $this->apple_legacy_icons_list as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
					</ul>
				</span>
			</div>
		<?php
	}


	/**
	 * Apple HD size
	 *
	 * @version 1.0.4
	 * @since 1.0.2
	 */
	public function apple_hd_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_apple_settings[apple_hd_active]" value="1" <?php checked( $this->apple_hd_active ); ?> />
			<div class="tooltip">
				<span class="tooltiptext tooltip-right">
					<ul>
						<?php foreach ( $this->apple_hd_icons_list as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
					</ul>
				</span>
			</div>
		<?php
	}


	/**
	 * Register Metro admin page options
	 *
	 * @version 1.0.6
	 * @since 1.0.5
	 * @deprecated in 1.0.9
	 * @see self::metro_admin_form()
	 */
	public function add_metro_settings()
	{
		parent::fetch_data();

		// Add Section for option fields
		add_settings_section( 'hblam_metro_section', __( 'Metro settings', 'hbalm' ), array( $this, 'null_cb' ), 'hbalm_metro' ); // id, title, display cb, page


		// Metro Tile Color
		add_settings_field( 'metro_tile_color', __( 'Tile bg color', 'hbalm' ), array( $this, 'add_color_input' ), 'hbalm_metro', 'hblam_metro_section', array('section' => 'hbalm_metro_settings', 'setting' => 'metro_tile_color') ); // id, title, display cb, page, section, args

		// Metro HD icons
		add_settings_field( 'metro_hd_icon_active', __( 'Metro HD icons', 'hbalm' ), array( $this, 'metro_hd_icon_active_input' ), 'hbalm_metro', 'hblam_metro_section' );

		// Metro wide icon
		add_settings_field( 'metro_wide_icon_active', __( 'Metro wide icon', 'hbalm' ), array( $this, 'metro_wide_icon_active_input' ), 'hbalm_metro', 'hblam_metro_section' );
		add_settings_field( 'metro_wide_icon_image', __( 'Metro wide icon image', 'hbalm' ), array( $this, 'metro_wide_icon_image_input' ), 'hbalm_metro', 'hblam_metro_section' );

		// Force Metro Tile icon <meta> in HTML
		add_settings_field( 'metro_html_tile_active', __( 'Force Metro TileImage metateg in HTML code', 'hbalm' ), array( $this, 'metro_html_tile_active_input' ), 'hbalm_metro', 'hblam_metro_section' );

		// browserconfig.xml file
		add_settings_field( 'browserconfig_active', __( 'Create IE/ Edge browserconfig.xml file', 'hbalm' ), array( $this, 'browserconfig_active_input' ), 'hbalm_metro', 'hblam_metro_section' );


		// Register Settings
		register_setting( 'hbalm_metro', 'hbalm_metro_settings', array( $this, 'validate_metro_options' ) ); // option group, option name, sanitize cb
	}


	/**
	 * Metro Tile Color Input
	 *
	 * @since 1.0.5
	 * @deprecated in 1.0.6
	 * @see add_color_input()
	 */
	public function metro_tile_color_input()
	{
		?>
			<input type="text" name="hbalm_metro_settings[metro_tile_color]" value="<?php echo $this->metro_tile_color; ?>" class="hbalm-colorpicker" />
		<?php
	}


	/**
	 * Metro HD icons
	 *
	 * @version 1.0.5
	 * @since 1.0.2
	 */
	public function metro_hd_icon_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_metro_settings[metro_hd_icon_active]" value="1" <?php checked( $this->metro_hd_icon_active ); ?> />
			<div class="tooltip">
				<span class="tooltiptext tooltip-right">
					<ul>
						<li><strong>HD</strong></li>
						<?php foreach ( $this->metro_hd_icons_sizes as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
						<li><hr /></li>
						<li><strong>SD</strong></li>
						<?php foreach ( $this->metro_sd_icons_sizes as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
					</ul>
				</span>
			</div>
		<?php
	}


	/**
	 * Metro wide icon
	 *
	 * @version 1.0.9
	 * @since 1.0.2
	 */
	public function metro_wide_icon_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_metro_settings[metro_wide_icon_active]" id="metro_wide_icon_active" value="1" <?php checked( $this->metro_wide_icon_active ); ?> />
			<div class="tooltip">
				<span class="tooltiptext tooltip-right">
					<ul>
						<?php foreach ( $this->metro_hd_wide_icons_list as $key => $values ): ?>
							<li><?php echo $key; ?>px</li>
						<?php endforeach; ?>
					</ul>
				</span>
			</div>
		<?php
	}


	/**
	 * Metro wide icon
	 *
	 * @version 1.0.10
	 * @since 1.0.2
	 */
	public function metro_wide_icon_image_input()
	{
		?>
			<input type="text" name="hbalm_metro_settings[metro_wide_icon_image]" class="image_path" value="<?php echo $this->metro_wide_icon_image; ?>" id="metro_wide_icon_image" readonly="readonly" style="display: none;" />
			<input type="button" value="<?php _e('Upload Image', 'hbalm'); ?>" class="button-primary" id="metro_wide_icon_image_btn" /> <?php _e('Upload your Image from here', 'hbalm'); ?>

			<div id="show_upload_preview">
				<?php if ( !empty($this->metro_hd_wide_icons_list) ): ?>
					<img src="<?php echo $this->metro_wide_icon_image ; ?>" id="metro_wide_icon_image_preview" />
					<input type="submit" name="remove" value="<?php _e('Remove Image', 'hbalm'); ?>" class="button-secondary" id="remove_image" />
				<?php endif; ?>
			</div>
		<?php
	}


	/**
	 * Metro Tile icons in HTML
	 *
	 * Forces the html <meta> for tile icon
	 *
	 * @version 1.0.5
	 * @since 1.0.3
	 */
	public function metro_html_tile_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_metro_settings[metro_html_tile_active]" value="1" <?php checked( $this->metro_html_tile_active ); ?> />
		<?php
	}


	/**
	 * IE/ Edge browserconfig.xml active file
	 *
	 * @version 1.0.5
	 * @since 1.0.2
	 */
	public function browserconfig_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_metro_settings[browserconfig_active]" value="1" <?php checked( $this->browserconfig_active ); ?> />
		<?php
	}


	/**
	 * Register Metro admin page options
	 *
	 * @version 1.0.6
	 * @since 1.0.5
	 * @deprecated in 1.0.9
	 * @see self::chrome_admin_form()
	 */
	public function add_chrome_settings()
	{
		parent::fetch_data();

		// Add Section for option fields
		add_settings_section( 'hblam_chrome_section', __( 'Chrome settings', 'hbalm' ), array( $this, 'null_cb' ), 'hbalm_chrome' ); // id, title, display cb, page


		// Chrome Background Color Field
		add_settings_field( 'chrome_bg_color', __( 'Chrome bg color', 'hbalm' ), array( $this, 'add_color_input' ), 'hbalm_chrome', 'hblam_chrome_section', array('section' => 'hbalm_chrome_settings', 'setting' => 'chrome_bg_color') ); // id, title, display cb, page, section, args

		// Chrome Color Field
		add_settings_field( 'chrome_color', __( 'Chrome app color', 'hbalm' ), array( $this, 'add_color_input' ), 'hbalm_chrome', 'hblam_chrome_section', array('section' => 'hbalm_chrome_settings', 'setting' => 'chrome_color') ); // id, title, display cb, page, section, args

		// site.webmanifest file
		add_settings_field( 'chrome_webmanifest_active', __( 'Create Chrome mobile webmanifest file', 'hbalm' ), array( $this, 'chrome_webmanifest_active_input' ), 'hbalm_chrome', 'hblam_chrome_section' );

		// Chrome Display Type
		add_settings_field( 'chrome_orientation', __( 'Chrome app orientation', 'hbalm' ), array( $this, 'chrome_orientation_input' ), 'hbalm_chrome', 'hblam_chrome_section' ); // id, title, display cb, page, section

		// Chrome Orientation
		add_settings_field( 'chrome_display_type', __( 'Chrome app display type', 'hbalm' ), array( $this, 'chrome_display_type_input' ), 'hbalm_chrome', 'hblam_chrome_section' ); // id, title, display cb, page, section


		// Register Settings
		register_setting( 'hbalm_chrome', 'hbalm_chrome_settings', array( $this, 'validate_chrome_options' ) ); // option group, option name, sanitize cb
	}


	/**
	 * Chrome webmanifest file values active input
	 *
	 * @version 1.0.9
	 * @since 1.0.3
	 */
	public function chrome_webmanifest_active_input()
	{
		?>
			<input type="checkbox" name="hbalm_chrome_settings[chrome_webmanifest_active]" id="chrome_webmanifest_active" value="1" <?php checked( $this->chrome_webmanifest_active ); ?> />
		<?php
	}


	/**
	 * Chrome webmanifest display type select field
	 *
	 * @version 1.0.7
	 * @since 1.0.6
	 */
	public function chrome_display_type_input()
	{
		?>
			<select name="hbalm_chrome_settings[chrome_display_type]">
				<?php foreach ( $this->chrome_display_type_values as $value => $name ): ?>
					<option value="<?php echo $value; ?>" <?php selected( $this->chrome_display_type, $value ); ?>><?php echo $name; ?></option>
				<?php endforeach; ?>
			</select>
		<?php
	}


	/**
	 * Chrome webmanifest orientation select field
	 *
	 * @version 1.0.7
	 * @since 1.0.6
	 */
	public function chrome_orientation_input()
	{
		?>
			<select name="hbalm_chrome_settings[chrome_orientation]">
				<?php foreach ( $this->chrome_orientation_values as $value => $name ): ?>
					<option value="<?php echo $value; ?>" <?php selected( $this->chrome_orientation, $value ); ?>><?php echo $name; ?></option>
				<?php endforeach; ?>
			</select>
		<?php
	}


	/**
	 * NULL callback function
	 *
	 * @since 1.0.2
	 */
	public function null_cb()
	{
		/* Leave blank, we will use submenu page */
	}


	/**
	 * Admin pages filters
	 *
	 * @version 1.0.10
	 * @since 1.0.2
	 * @deprecated in 1.0.10 - Now using wp.media() to upload, select and/ or crop images
	 */
	public function add_admin_filters()
	{
		// Check the pages that we are working on are the ones used by the plugin
		global $pagenow;

		if ( 'media-upload.php' == $pagenow
			|| 'async-upload.php' == $pagenow )
		{
			add_filter('gettext', array($this, 'replace_window_text'), 1, 2);
			add_filter('attachment_fields_to_edit', array($this, 'unset_thickbox_fields'), 10, 2);
		}
	}


	/**
	 * Replace ThickBox texts
	 *
	 * @version 1.0.10
	 * @since 1.0.2
	 * @deprecated in 1.0.10 - Now using wp.media() to upload, select and/ or crop images
	 */
	public function replace_window_text( $translated_text, $text )
	{
		if ( 'Required fields are marked %s' == $text )
		{
			return '';
		}

		if ( 'Insert into Post' == $text )
		{
			$referer = strpos(wp_get_referer(), 'media_page');
			if ( $referer != '' )
			{
				return __('Select Image', 'hbalm');
			}
		}

		return $translated_text;
	}


	/**
	 * Unsets ThickBox upload options
	 *
	 * @since 1.0.10
	 * @deprecated in 1.0.10 - Now using wp.media() to upload, select and/ or crop images
	 */
	public function unset_thickbox_fields( $form_fields, $post )
	{
		unset(
			$form_fields['post_title'],
			$form_fields['url'],
			$form_fields['image_alt'], 
			$form_fields['post_excerpt'], 
			$form_fields['post_content'], 
			$form_fields['align'], 
			$form_fields['image-size']
		);

		// Create the url input and set display to none
		$html = "<div style='display: none;'><input type='text' class='text urlfield' name='attachments[$post->ID][url]' value='$post->guid'></div>";
		// Set as image-size field
		$form_fields['url'] = array(
			'label' => '', //leave with no label
			'input' => 'html', //input type is html
			'html' => $html //the actual html
		);


		// Create the size input for wide tile only and set display to none
		$size = ( $this->metro_hd_icon_active ? $this->metro_hd_wide_icons_list : $this->metro_sd_wide_icons_list );
		$css_id = "image-size-{$size}-{$post->ID}";
		$html = "<div style='display: none;' class='image-size-item'><input type='radio' name='attachments[$post->ID][image-size]' id='{$css_id}' value='{$size}'checked='checked' /></div>";
		// Set as image-size field
		$form_fields['image-size'] = array(
			'label' => '', //leave with no label
			'input' => 'html', //input type is html
			'html' => $html //the actual html
		);


		return $form_fields;
	}


	/**
	 * Validate general form fields
	 *
	 * @version 1.0.9
	 * @since 1.0.2
	 * @param $field (array)
	 * @return (array) apply_filters()
	 */
	public function validate_base_options( $fields )
	{
		check_admin_referer( 'update_settings', 'hbalm_nonce' );

		parent::fetch_data();
		// parent::_debug($fields,1);

		$valid_fields = array();


		// Default HD
		$valid_fields['default_hd_active'] = self::validate_checkbox_field($fields, 'default_hd_active');

		// Colors
		$valid_fields = array_merge($valid_fields, $this->validate_color_fields($fields) );

		// Default webmanifest
		$valid_fields['default_webmanifest_active'] = self::validate_checkbox_field($fields, 'default_webmanifest_active');

		if ( false === parent::save_webmanifest_file( $valid_fields ) )
		{
			self::enqueue_settings_error( 'default_webmanifest_active', __('was not updated due to permission issues with the file') );
			$valid_fields['default_webmanifest_active'] = $this->default_webmanifest_active;
		}


		self::add_settings_messages();

		return apply_filters( 'validate_options', $valid_fields, $fields);
	}


	/**
	 * Validate Apple form fields
	 *
	 * @version 1.0.9
	 * @since 1.0.4
	 * @param $field (array)
	 * @return (array) apply_filters()
	 */
	public function validate_apple_options( $fields )
	{
		check_admin_referer( 'update_settings', 'hbalm_nonce' );

		parent::fetch_data();
		// parent::_debug($fields,1);

		$valid_fields = array();

		// Apple Display Type
		$valid_fields['apple_display_type'] = self::validate_select_field($fields, 'apple_display_type');

		// Apple legacy
		$valid_fields['apple_legacy_active'] = self::validate_checkbox_field($fields, 'apple_legacy_active');

		// Apple HD
		$valid_fields['apple_hd_active'] = self::validate_checkbox_field($fields, 'apple_hd_active');

		// Colors
		$valid_fields = array_merge($valid_fields, $this->validate_color_fields($fields) );


		self::add_settings_messages();

		return apply_filters( 'validate_options', $valid_fields, $fields);
	}


	/**
	 * Validate Metro form fields
	 *
	 * @version 1.0.10
	 * @since 1.0.5
	 * @param $field (array)
	 * @return (array) apply_filters()
	 */
	public function validate_metro_options( $fields )
	{
		check_admin_referer( 'update_settings', 'hbalm_nonce' );

		parent::fetch_data();
		// parent::_debug($fields,1);

		$valid_fields = array();


		// Metro HD Icon
		$valid_fields['metro_hd_icon_active'] = self::validate_checkbox_field($fields, 'metro_hd_icon_active');

		// Metro Wide Icon
		$valid_fields['metro_wide_icon_active'] = self::validate_checkbox_field($fields, 'metro_wide_icon_active');
		$valid_fields['metro_wide_icon_image'] = self::validate_image_field($fields, 'metro_wide_icon_image');
		if ( $valid_fields['metro_wide_icon_active']
			&& empty($valid_fields['metro_wide_icon_image']) )
		{
			// Do not allow to activate the checkbox if no image uploaded
			self::enqueue_settings_error( 'metro_wide_icon_active', __('was deactivated if metro_wide_icon_image input is empty') );
			$valid_fields['metro_wide_icon_active'] = 0;
		}

		// Metro HTML Tile Icon
		$valid_fields['metro_html_tile_active'] = self::validate_checkbox_field($fields, 'metro_html_tile_active');

		// Colors
		$valid_fields = array_merge($valid_fields, $this->validate_color_fields($fields) );

		// browserconfig.xml
		$valid_fields['browserconfig_active'] = self::validate_checkbox_field($fields, 'browserconfig_active');
		if ( $valid_fields['browserconfig_active'] )
		{
			if ( false === parent::save_browserconfig_file() )
			{
				self::enqueue_settings_error( 'browserconfig_active', __('was not updated due to permission issues with the file') );
				$valid_fields['browserconfig_active'] = 0;
			}
		}


		self::add_settings_messages();

		return apply_filters( 'validate_options', $valid_fields, $fields);
	}


	/**
	 * Validate Chrome form fields
	 *
	 * Do not allow activation if not HTTPS server
	 *
	 * @version 1.0.10
	 * @since 1.0.5
	 * @param $field (array)
	 * @return (array) apply_filters()
	 */
	public function validate_chrome_options( $fields )
	{
		check_admin_referer( 'update_settings', 'hbalm_nonce' );

		parent::fetch_data();
		// parent::_debug($fields);

		$valid_fields = array();


		// Chrome display type
		$valid_fields['chrome_display_type'] = self::validate_select_field($fields, 'chrome_display_type');

		// Chrome chrome_orientation
		$valid_fields['chrome_orientation'] = self::validate_select_field($fields, 'chrome_orientation');

		// Colors
		$valid_fields = array_merge($valid_fields, $this->validate_color_fields($fields) );

		// Chrome site.webmanifest
		$valid_fields['chrome_webmanifest_active'] = self::validate_checkbox_field($fields, 'chrome_webmanifest_active');

		if ( empty($_SERVER['HTTPS']) )
		{
			if ( $valid_fields['chrome_webmanifest_active'] )
			{
				self::enqueue_settings_error( 'chrome_webmanifest_active', __('was not updated. You need HTTPS to set up a webapp') );
				$valid_fields['chrome_webmanifest_active'] = 0;
			}
		}
		else
		{
			if ( false === parent::save_webmanifest_file( $valid_fields ) )
			{
				self::enqueue_settings_error( 'chrome_webmanifest_active', __('was not updated due to permission issues with the file') );
				$valid_fields['chrome_webmanifest_active'] = 0;
			}
		}


		self::add_settings_messages();

		return apply_filters( 'validate_options', $valid_fields, $fields);
	}


	/**
	 * Validate $name checkbox field content, if not
	 *
	 * @since 1.0.9
	 * @param $fields (array)
	 * @param $name   (string)
	 * @return (boolean)
	 */
	private function validate_select_field( $fields, $name )
	{
		if ( !isset($this->{$name.'_values'})
			|| !is_array($this->{$name.'_values'}) )
		{
			return '';
		}

		$new_value = isset($fields[$name]) && array_key_exists($fields[$name], $this->{$name.'_values'} ) ? trim( $fields[$name] ) : $this->{$name};

		if ( $new_value != $this->{$name} )
		{
			self::enqueue_success_msg( $name );
		}


		return strip_tags( stripslashes( $new_value ) );
	}


	/**
	 * Validate $name checkbox field content
	 *
	 * @since 1.0.7
	 * @param $fields (array)
	 * @param $name   (string)
	 * @return (boolean)
	 */
	private function validate_checkbox_field( $fields, $name )
	{
		$active = isset($fields[$name]) ? 1 : 0;

		if ( $active != $this->{$name} )
		{
			self::enqueue_success_msg( $name );
		}

		return (boolean) strip_tags( stripslashes( $active ) );
	}


	/**
	 * Validate $name image field content
	 *
	 * @since 1.0.7
	 * @param $fields (array)
	 * @param $name   (string)
	 * @return (boolean)
	 */
	private function validate_image_field( $fields, $name )
	{
		$image = isset($fields[$name]) ? trim( $fields[$name] ) : '';


		return strip_tags( stripslashes( $image ) );
	}


	/**
	 * Validate color fields matching "_color" name
	 *
	 * @version 1.0.7
	 * @since 1.0.4
	 * @param $fields (array)
	 * @return (array) valid_fields
	 */
	private function validate_color_fields( $fields )
	{
		$valid_fields = array();

		foreach ( $fields as $key => $value )
		{
			if ( preg_match('/_color/', $key) )
			{
				$color[$key] = trim( $value );
				$color[$key] = strip_tags( stripslashes( $color[$key] ) );

				// Check if is a valid hex color
				if ( FALSE === $this->is_hex_color( $color[$key] ) )
				{
					$required = false;
					if ( $key == "default_bg_color" )
					{
						$error_text = __("Insert a valid color for Main Background Color", "hbalm");
						$required = true;
					}

					if ( $key == "metro_tile_color" ) $error_text = __("Insert a valid color for Metro Tile Color", "hbalm");
					if ( $key == "apple_bg_color" )   $error_text = __("Insert a valid color for Apple Background Color", "hbalm");
					if ( $key == "chrome_bg_color" )  $error_text = __("Insert a valid color for Chrome Background Color", "hbalm");
					if ( $key == "chrome_color" )     $error_text = __("Insert a valid color for Chrome Theme Color", "hbalm");



					// Accept an empty value if it's not required
					if ( !$required && $color[$key] == '' )
					{
						$valid_fields[$key] = '';
						continue;
					}

					// Set the error message
					self::enqueue_settings_error( $key, $error_text );

					// Get the previous valid value
					$valid_fields[$key] = $this->{$key};
				}
				else
				{
					$valid_fields[$key] = $color[$key];
				}
			}
		}

		return $valid_fields;
	}


	/**
	 * Check if value is a valid HEX color
	 *
	 * Must start with # and have 6 digits
	 *
	 * @since 1.0.2
	 * @param $hex_color (string)
	 * @return (boolean)
	 */
	private function is_hex_color( $hex_color )
	{
		if ( preg_match( '/^#[a-f0-9]{6}$/i', $hex_color ) )
		{
			return true;
		}
		
		return false;
	}
}


/**
 * Exceptions
 *
 * @since 1.0.2
 */
class HBALM_exception extends Exception {}


/**
 * WP hooks
 *
 * @version 1.0.10
 * @since 1.0.2
 */
function hbalm_init_plugin()
{
	$browser_app_links_metas = new Admin_Head_Browser_App_Links_Metas();
}
add_action( 'plugins_loaded', 'hbalm_init_plugin' );

register_uninstall_hook( HBALM_FILE, array( 'Admin_Head_Browser_App_Links_Metas', 'hblam_uninstall' ) );
register_activation_hook( HBALM_FILE, array( 'Admin_Head_Browser_App_Links_Metas', 'hbalm_activation' ) );