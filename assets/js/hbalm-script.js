(function($){

	$(function() {
		/* Add wpColorPicker */
		$('.hbalm-colorpicker').wpColorPicker();

		/* webmanifest options */
		$('#chrome_webmanifest_active').change(function() {
			var chromeContainer = $('.chrome-webmanifest-options-container');

			$(this).prop("checked", this.checked);

			if ( this.checked ) {
				chromeContainer.slideDown('slow');
			} else {
				chromeContainer.slideUp('slow');
			}
		});

		/* wide tile image */
		$('#metro_wide_icon_active').change(function() {
			var tileContainer = $('.metro-wide-tile-image-container');

			$(this).prop("checked", this.checked);

			if ( this.checked ) {
				tileContainer.slideDown('slow');
			} else {
				tileContainer.slideUp('slow');
			}
		});
	});

})(jQuery);