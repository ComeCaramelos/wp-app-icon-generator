# App icon generator
 * __Author:__ [Roberto Solano](https://comecaramelos.es)
 * __Version:__ 1.1.2
 * __Description:__ Configure site icons for Chrome, iOS/Safari, IE/Edge/Metro and Firefox (among others) using _site\_icon_ or _custom\_logo_ images for WordPress and sets up your site as a standalone mobile application
 * __Namespace:__ hbalm


## Installation
Copy the folder _hbalm_ into your site plugins folder (``/wp-content/plugins/``) and activate it

## Usage
Upload a site icon or custom logo (if you haven't yet) and set the desired sizes and colors inside plugin options page.

_Site icon value usage has priority over custom logo value_

## Changelog

### 1.1.2
 * Fixed an error when adding application to desktop chrome

### 1.1.1
 * Fixed an error causing translations not loading until plugin activation

### 1.1.0
 * i18n:
     * es_ES
     * en
 * Minify _.css_ and _.js_ files
 * Updated plugin description

### 1.0.10
 * Metro wide image fixed dimensions, now using _wp.media()_ instead of _tb\_show()_
 * Updated media queries
 * Updated form help tooltips
 * Plugin activation and uninstall functions
 * Deleted duplicated class file
 * Error/ success messages functions renamed
 * Fixed an error causing not saving correctly the color values to _browserconfig.xml_ and _site.webmanifest_ files

### 1.0.9
 * Styled forms
 * Fixed color fields type value
 * Removed default WordPress icons tags from header to avoid conflicts
 * Added WP nonce verification
 * Chrome and Metro forms show/ hide elements dynamically
 * Functions name update

### 1.0.8
 * Advanced file managing (check, edit & create)
 * Prevent Chrome _site.webmanifest_ active if not HTTPS server
 * Fixed an error on WPColorPicker register
 * Fixed an error for some field not displaying the current setting value

### 1.0.7
 * Admin error & success messages enhanced

### 1.0.6
 * New plugin name
 * Plugin description translation ready
 * Added _site.webmanifest_ control for Firefox (_General settings page_) in addition to Chrome webmanifest values
 * Added specific sizes for _site.webmanifest_ file icons
 * Individual color input functions are now deprecated, using _add_color_input()_ instead
 * Moved some sizes from _Apple legacy icons_ to default _Apple touch icons_
 * More options in Admin panel:
     * Firefox _site.webmanifest_ file values _On/ Off_
     * Chrome App Color and Background values in addition to Default bg color
     * Chrome App Display Type _Standalone/ Browser_
     * Chrome App Orientation _Unset/ Portrait/ Landscape_
     * iOS App Display Type _Standalone/ Browser_
     * Force Metro TileIcon HTML ``<meta>`` _On/ Off_

### 1.0.5
 * Admin options ordered by pages (continue)

### 1.0.4
 * Admin options ordered by pages
 * Callbacks for help in the sidebar of administration pages

### 1.0.3
 * Extended class for admin panel
 * More options in Admin panel:
     * Force Metro TileIcon HTML ``<meta>`` _On/ Off_
     * Chrome Mobile Webmanifest _On/ Off_

### 1.0.2
 * __Head Browser App Metas__ is now a WordPress plugin
 * More options in Admin panel:
     * HD favicons _On/ Off_
     * Legacy & HD Apple icons _On/ Off_
     * HD Metro icons _On/ Off_
     * Metro wide icon _Image uploader_
     * IE/ Edge _browserconfig.xml_ file _On/ Off_

### 1.0.1
 * Now the _get_ methods returns the HTML string generated
 * No need to register icon sizes as WordPress image sizes
 * _Readme.md_ tweaked

### 1.0.0
 * First version